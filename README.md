# 一起玩微信小程序

#### 介绍
这是一个方便当代年轻人享受新社交模式，能在该平台寻找到自己志同道合的好伙伴，在此程序上，寻找您的好朋友，不担心以后活动没人陪你一起玩

#### 软件架构
软件架构说明


#### 安装教程

1. 先在resource文件中，db文件夹中运行tplay.sql

   ![image-20221214202314649](tplay\src\main\resources\static\images\md_image01.png)

2. 修改配置文件中数据库配置，改成您的密码与账号哦！

   其中dev是开发环境，test是测试环境，方便随时更换

   ![image-20221214202549776](tplay\src\main\resources\static\images\md_image02.png)

3. 若使用的是dev开发环境，请在配置文件开头配置您本地的图片存储路径

   ![image-20221214202658420](tplay\src\main\resources\static\images\md_image03.png)

4. 如果在test环境下，将云服务器信息配置为您自己的

   ![image-20221214202840794](tplay\src\main\resources\static\images\md_image04.png)

   5. 别忘记了也要配置图片路径哦！

## 启动

​	在SpringBoot主启动类启动即可！

   

   

