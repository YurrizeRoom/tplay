package com.yurrize;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yurrize.dto.WantPlayDto;
import com.yurrize.entity.Play;
import com.yurrize.entity.User;
import com.yurrize.entity.WantPlayer;
import com.yurrize.mapper.UserMapper;
import com.yurrize.service.IPlayService;
import com.yurrize.service.IPlayTypeService;
import com.yurrize.service.IUserService;
import com.yurrize.service.IWantPlayService;
import com.yurrize.service.impl.UserServiceImpl;
import com.yurrize.utils.CommonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.yurrize.utils.SystemConstants.MAX_PAGE_SIZE;


@RunWith(SpringRunner.class)
@SpringBootTest
public class test {


    @Resource
    private IUserService userService;

    @Resource
    private IPlayTypeService playTypeService;

    @Resource
    private IWantPlayService wantPlayService;

    @Resource
    private IPlayService playService;

    @Resource
    private UserMapper userMapper;


    @Test
    public void test(){

        //微信的接口


        String url = "https://api.weixin.qq.com/sns/jscode2session?appid="+APPID+
                "&secret="+SECRET+"&js_code="+ code +"&grant_type=authorization_code";
        RestTemplate restTemplate = new RestTemplate();
        //进行网络请求,访问url接口
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        //根据返回值进行后续操作
        if(responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK)
        {
            //解析从微信服务器获得的openid和session_key;
            System.out.println(responseEntity);
            //下面就可以写自己的业务代码了
            //最后要返回一个自定义的登录态,用来做后续数据传输的验证
        }


    }


    @Test
    public void loginTest(){

        User user = userService.query().eq("openid", openId).one();
        System.out.println(user);
    }

    @Test
    public void queryTest(){
//        boolean flag = playTypeService.query().eq("id", 5).exists();
//        System.out.println(flag);

        // 根据用户排序
        Page<Play> page = playService.query()
                .orderByDesc("create_time")
                .page(new Page<>(1, MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Play> records = page.getRecords();
        for (Play record : records) {
            List<String> collect = Arrays.stream(record.getImages().split(",")).collect(Collectors.toList());
            System.out.println(collect);
        }

    }

    @Test
    public void view(){
        String url="https://api.inews.qq.com/newsqa/v1/query/inner/publish/modules/list?modules=statisGradeCityDetail,diseaseh5Shelf";

        RestTemplate restTemplate = new RestTemplate();

        String response = restTemplate.getForObject(url, String.class);


        JSONObject jsonObject = new JSONObject(response);

        Object data = jsonObject.get("data");


        System.out.println(data);

    }

    @Test
    public void loginTest1(){
        List<Play> user_id = playService.query().eq("user_id", 1).list();
        System.out.println(user_id);
    }


    @Test
    public void  wantTest(){
//        List<WantPlayer> list = wantPlayService.query().eq("play_id", 1).list();
//        System.out.println(list);

        WantPlayer wantPlayer = new WantPlayer();
        wantPlayer.setId(1L);
        wantPlayer.setStatus(2);

        boolean b = wantPlayService.updateById(wantPlayer);
        System.out.println(b);
    }

    @Test
    public void testPlay(){
        Play byId = playService.getById(1);
        System.out.println(byId);
    }

    @Test
    public void listPlayTest(){
        WantPlayer one = wantPlayService.query().eq("play_id", 1).one();
        System.out.println(one);
    }
}
