

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_blog
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog`;
CREATE TABLE `tb_blog`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `play_id` bigint(20) NOT NULL COMMENT '活动id',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `images` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动的照片，最多9张，多张以\",\"隔开',
  `content` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '活动心得',
  `liked` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '点赞数量',
  `comments` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '评论数量',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog
-- ----------------------------
INSERT INTO `tb_blog` VALUES (1, 1, 4, '深圳狼人杀交友活动', '/imgs/blogs/31/sadas.jpg', '本周的狼人杀活动是在深圳大剧院附近一家卓有关举办的。我组织的狼人杀游戏主要以交友为主，游戏为辐，对玩家的游戏水平要求是非常低的，所以非常换了。类似的活动我经常会组织，欢饮在下方评论+点赞哦', 29, 214, '2022-11-09 16:40:24', '2022-11-09 18:07:04');
INSERT INTO `tb_blog` VALUES (2, 2, 1, '消暑茶话会', '/imgs/blogs/6/8.jpg', '茫茫露露的时光虽然匆匆，但不虚度，今天下公司的茶话会认真倾听大家的故事和分享，发现了更多的宝藏同事，每个人身上都有各自的发光点，自己也在刺戳洞中收获了满足开心和快乐', 1, 2, '2022-11-09 16:26:06', '2022-11-09 17:53:56');
INSERT INTO `tb_blog` VALUES (4, 13, 2, '无尽浪漫的夜晚丨在万花丛中摇晃着红酒杯🍷品战斧牛排🥩', '/imgs/blogs/7/14/4771fefb-1a87-4252-816c-9f7ec41ffa4a.jpg,/imgs/blogs/4/10/2f07e3c9-ddce-482d-9ea7-c21450f8d7cd.jpg,/imgs/blogs/2/6/b0756279-65da-4f2d-b62a-33f74b06454a.jpg,/imgs/blogs/10/7/7e97f47d-eb49-4dc9-a583-95faa7aed287.jpg,/imgs/blogs/1/2/4a7b496b-2a08-4af7-aa95-df2c3bd0ef97.jpg,/imgs/blogs/14/3/52b290eb-8b5d-403b-8373-ba0bb856d18e.jpg', '生活就是一半烟火·一半诗意\\n手执烟火谋生活·心怀诗意以谋爱·\\n当然\\n\r\n男朋友给不了的浪漫要学会自己给?\\n\r\n无法重来的一生·尽量快乐.\\n?「小筑里·神秘浪漫花园餐厅」\\n\r\n?这是一家最最最美花园的西餐厅·到处都是花餐桌上是花前台是花  美好无处不在\r\n品一口葡萄酒，维亚红酒马瑟兰·微醺上头工作的疲惫消失无际·生如此多娇?\\n地址:延安路200号(家乐福面)\\n交通:地铁①号线定安路B口出右转过下通道右转就到啦～\\n--------------?菜品详情?---------------\\n「战斧牛排]\\n\r\n超大一块战斧牛排经过火焰的炙烤发出阵阵香，外焦里嫩让人垂涎欲滴，切开牛排的那一刻，牛排的汁水顺势流了出来，分熟的牛排肉质软，简直细嫩到犯规，一刻都等不了要放入嘴里咀嚼～\\n「奶油培根意面」\\n太太太好吃了?\\n我真的无法形容它的美妙，意面混合奶油香菇的香味真的太太太香了，我真的舔盘了，一丁点美味都不想浪费‼️\\n「香菜汁烤鲈鱼」\\n这个酱是辣的 真的绝好吃‼️\\n鲈鱼本身就很嫩没什么刺，烤过之后外皮酥酥的，鱼肉蘸上酱料根本停不下来啊啊啊啊\\n能吃辣椒的小伙伴一定要尝尝\\n非常可 好吃子?\r\n\\n--------------?个人感受?---------------\\n服务\\n小姐姐特别耐心的给我们介绍彩票 \\n推荐特色菜品，拍照需要帮忙也是尽心尽力配合，太爱他们了\\n【?环境】\\n比较有格调的西餐厅 整个餐厅的布局可称得上的万花丛生 有种在人间仙境的感觉?\\n集美食美酒与鲜花为一体的风格店铺 令人向往\\n烟火皆是生活 人间皆是浪漫\\n', 2, 104, '2021-12-28 19:50:01', '2022-11-19 16:34:10');
INSERT INTO `tb_blog` VALUES (5, 11, 2, '人均30💰杭州这家港式茶餐厅我疯狂打call‼️', '/imgs/blogs/4/7/863cc302-d150-420d-a596-b16e9232a1a6.jpg,/imgs/blogs/11/12/8b37d208-9414-4e78-b065-9199647bb3e3.jpg,/imgs/blogs/4/1/fa74a6d6-3026-4cb7-b0b6-35abb1e52d11.jpg,/imgs/blogs/9/12/ac2ce2fb-0605-4f14-82cc-c962b8c86688.jpg,/imgs/blogs/4/0/26a7cd7e-6320-432c-a0b4-1b7418f45ec7.jpg,/imgs/blogs/15/9/cea51d9b-ac15-49f6-b9f1-9cf81e9b9c85.jpg', '又吃到一家好吃的茶餐厅🍴环境是怀旧tvb港风📺边吃边拍照片📷几十种菜品均价都在20+💰可以是很平价了！<br>·<br>店名：九记冰厅(远洋店)<br>地址：杭州市丽水路远洋乐堤港负一楼（溜冰场旁边）<br>·<br>✔️黯然销魂饭（38💰）<br>这碗饭我吹爆！米饭上盖满了甜甜的叉烧 还有两颗溏心蛋🍳每一粒米饭都裹着浓郁的酱汁 光盘了<br>·<br>✔️铜锣湾漏奶华（28💰）<br>黄油吐司烤的脆脆的 上面洒满了可可粉🍫一刀切开 奶盖流心像瀑布一样流出来  满足<br>·<br>✔️神仙一口西多士士（16💰）<br>简简单单却超级好吃！西多士烤的很脆 黄油味浓郁 面包体超级柔软 上面淋了炼乳<br>·<br>✔️怀旧五柳炸蛋饭（28💰）<br>四个鸡蛋炸成蓬松的炸蛋！也太好吃了吧！还有大块鸡排 上淋了酸甜的酱汁 太合我胃口了！！<br>·<br>✔️烧味双拼例牌（66💰）<br>选了烧鹅➕叉烧 他家烧腊品质真的惊艳到我！据说是每日广州发货 到店现烧现卖的黑棕鹅 每口都是正宗的味道！肉质很嫩 皮超级超级酥脆！一口爆油！叉烧肉也一点都不柴 甜甜的很入味 搭配梅子酱很解腻 ！<br>·<br>✔️红烧脆皮乳鸽（18.8💰）<br>乳鸽很大只 这个价格也太划算了吧， 肉质很有嚼劲 脆皮很酥 越吃越香～<br>·<br>✔️大满足小吃拼盘（25💰）<br>翅尖➕咖喱鱼蛋➕蝴蝶虾➕盐酥鸡<br>zui喜欢里面的咖喱鱼！咖喱酱香甜浓郁！鱼蛋很q弹～<br>·<br>✔️港式熊仔丝袜奶茶（19💰）<br>小熊🐻造型的奶茶冰也太可爱了！颜值担当 很地道的丝袜奶茶 茶味特别浓郁～<br>·', 1, 0, '2021-12-28 20:57:49', '2022-11-09 17:39:56');
INSERT INTO `tb_blog` VALUES (25, 7, 3, '新年茶话会', '/imgs', '正逢年末，公司组织举办了茶话会，加强了企业领导与员工之间的了解、合作和交流，进一步拉近彼此感情，创造员工互动，和谐的工作氛围。', 5, 12, '2022-11-09 16:30:32', '2022-11-09 17:56:52');
INSERT INTO `tb_blog` VALUES (26, 14, 5, '狼人杀冲冲冲', '/imgs/blogs/', '搜寻热爱玩狼人杀二点你，又或者喜欢但是不太会的你，都可以一起来玩呀。\r\n地址：广州市天河区天河新天地（地铁天河客运站D出口）', 3, 18, '2022-11-09 16:32:57', '2022-11-09 18:05:12');
INSERT INTO `tb_blog` VALUES (27, 4, 7, '血染狼人杀探店', '/imgs/blogs/123/sad,jpg', '一直很喜欢狼人杀这种发言游戏，后来血染升起简直日日夜夜，最近探得了一家新店，店家推荐了款血染版狼人杀，我天，把我惊呆了，这作者太聪明了居然能把这两者完美的融合一起，我们这桌9个人从晚上九点到店一直肝到了天亮，实在是太有趣了。', 25, 7, '2022-11-09 16:37:18', '2022-11-09 18:07:28');
INSERT INTO `tb_blog` VALUES (29, 3, 7, '真的要被这部电影笑疯，太太太喜欢了', '/imgs/blogs/', '电影名称：《追凶者也》，真的太喜欢这部影片了，上映至今刷过三次，每次愣是把我笑得人仰马翻，又笑点、有思考、全员演技在线、拍的真不错，强烈推荐哈哈哈。', 81000, 985, '2022-11-09 16:43:08', '2022-11-09 17:54:35');
INSERT INTO `tb_blog` VALUES (30, 9, 5, '办公室游园会玩法', '/imgs/nlogs', '年会季快到了各大活动场地定得如火如茶有什么方式可以花更少的钱办年会，还能办得员工都开开心心呢?\n当当当当~当然是计办公室游园会计啦,行政、HR们快来看看办公室游园会怎么办吧', 512, 21, '2022-11-09 16:45:35', '2022-11-09 18:02:27');
INSERT INTO `tb_blog` VALUES (31, 6, 6, '深圳展览·惰性时刻', '/imgs/blogs', '本次展览主要呈现的是“被破坏的秩序作品系列\n艺术家关音夫通过将颜料倒洒在画板上的手法让颜料随意流动形成的独特形状，带领我们共同走进了其抽象化风格的起源，抽象的图形构成与色彩叠加形成了一套语言的风貌，分享了他在艺术实践中与材料、色彩的持续博弈', 720, 14, '2022-11-09 16:47:53', '2022-11-09 18:02:36');
INSERT INTO `tb_blog` VALUES (32, 5, 8, '寸金桥公园之游乐园(最平价的游乐园）', '/imgs/', '摩天轮:\r\n情侣可以在摩天轮最高点拍照，恐高的话也可以在底下用俯拍角度拍摄.\r\n小型过山车:和小朋友或者伴侣朋友都可以去试试.\r\n空中飞船:有点高度，在上面转圈圈', 708, 54, '2022-11-09 16:50:59', '2022-11-09 18:00:21');

-- ----------------------------
-- Table structure for tb_blog_comments
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_comments`;
CREATE TABLE `tb_blog_comments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id',
  `blog_id` bigint(20) UNSIGNED NOT NULL COMMENT '朋友圈id',
  `parent_id` bigint(20) UNSIGNED NOT NULL COMMENT '关联的1级评论id，如果是一级评论，则值为0',
  `answer_id` bigint(20) UNSIGNED NOT NULL COMMENT '回复的评论id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '回复的内容',
  `liked` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '点赞数',
  `status` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '状态，0：正常，1：被举报，2：禁止查看',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_comments
-- ----------------------------
INSERT INTO `tb_blog_comments` VALUES (1, 3, 11, 0, 8, '鬼屋哪有npc 道具生锈架子抖看到了', 9, 0, '2022-11-09 17:01:32', '2022-11-09 17:01:32');
INSERT INTO `tb_blog_comments` VALUES (2, 8, 11, 1, 3, '我国庆取得时候是有的，工作人员戴着面具从同理出来', 3, 0, '2022-11-09 17:02:28', '2022-11-09 17:02:28');
INSERT INTO `tb_blog_comments` VALUES (3, 2, 11, 0, 8, '我感觉我去了一个假的寸金', 6, 0, '2022-11-09 17:03:04', '2022-11-09 17:03:04');
INSERT INTO `tb_blog_comments` VALUES (4, 8, 11, 1, 2, '里面有很多地方可以拍照的', 1, 0, '2022-11-09 17:03:47', '2022-11-09 17:03:47');
INSERT INTO `tb_blog_comments` VALUES (5, 7, 10, 0, 6, '周一有吗，闭不闭馆', 0, 0, '2022-11-09 17:04:45', '2022-11-09 17:04:45');
INSERT INTO `tb_blog_comments` VALUES (6, 6, 10, 1, 7, '周一闭馆', 0, 0, '2022-11-09 17:05:15', '2022-11-09 17:05:15');
INSERT INTO `tb_blog_comments` VALUES (7, 1, 7, 0, 7, '其他还好，在公交车上那一八张真的笑抽了', 928, 0, '2022-11-09 17:06:18', '2022-11-09 17:06:18');
INSERT INTO `tb_blog_comments` VALUES (8, 4, 7, 0, 7, '五星杀手，两天搞定！笑抽了', 116, 0, '2022-11-09 17:07:19', '2022-11-09 17:07:19');
INSERT INTO `tb_blog_comments` VALUES (9, 2, 7, 0, 7, '我看了，为啥一点都不好笑', 3, 0, '2022-11-09 17:07:50', '2022-11-09 17:07:50');
INSERT INTO `tb_blog_comments` VALUES (10, 7, 7, 1, 2, '喜剧电影，每个人get到的都不一样', 1, 0, '2022-11-09 17:08:24', '2022-11-09 17:08:24');

-- ----------------------------
-- Table structure for tb_follow
-- ----------------------------
DROP TABLE IF EXISTS `tb_follow`;
CREATE TABLE `tb_follow`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id',
  `follow_user_id` bigint(20) UNSIGNED NOT NULL COMMENT '关联的用户id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_follow
-- ----------------------------
INSERT INTO `tb_follow` VALUES (1, 2, 1, '2022-11-09 17:09:32');
INSERT INTO `tb_follow` VALUES (2, 2, 4, '2022-11-09 17:35:08');
INSERT INTO `tb_follow` VALUES (3, 3, 1, '2022-11-09 17:35:16');
INSERT INTO `tb_follow` VALUES (4, 3, 2, '2022-11-09 17:35:23');
INSERT INTO `tb_follow` VALUES (5, 3, 5, '2022-11-09 17:35:26');
INSERT INTO `tb_follow` VALUES (6, 3, 8, '2022-11-09 17:35:43');
INSERT INTO `tb_follow` VALUES (7, 2, 8, '2022-11-09 17:35:51');
INSERT INTO `tb_follow` VALUES (8, 1, 2, '2022-11-09 17:35:57');
INSERT INTO `tb_follow` VALUES (9, 1, 3, '2022-11-09 17:36:02');
INSERT INTO `tb_follow` VALUES (10, 1, 4, '2022-11-09 17:36:08');
INSERT INTO `tb_follow` VALUES (11, 1, 8, '2022-11-09 17:36:17');
INSERT INTO `tb_follow` VALUES (12, 4, 2, '2022-11-09 17:36:23');
INSERT INTO `tb_follow` VALUES (13, 4, 1, '2022-11-09 17:36:27');
INSERT INTO `tb_follow` VALUES (14, 5, 1, '2022-11-09 17:36:35');
INSERT INTO `tb_follow` VALUES (15, 5, 3, '2022-11-09 17:36:39');
INSERT INTO `tb_follow` VALUES (16, 5, 6, '2022-11-09 17:36:44');
INSERT INTO `tb_follow` VALUES (17, 5, 8, '2022-11-09 17:36:50');
INSERT INTO `tb_follow` VALUES (18, 7, 2, '2022-11-09 17:36:59');
INSERT INTO `tb_follow` VALUES (19, 8, 2, '2022-11-09 17:37:04');
INSERT INTO `tb_follow` VALUES (20, 7, 6, '2022-11-09 17:37:11');
INSERT INTO `tb_follow` VALUES (21, 7, 4, '2022-11-09 17:37:15');
INSERT INTO `tb_follow` VALUES (22, 7, 9, '2022-11-09 17:37:22');
INSERT INTO `tb_follow` VALUES (23, 7, 10, '2022-11-09 17:37:28');
INSERT INTO `tb_follow` VALUES (24, 9, 1, '2022-11-09 17:37:35');
INSERT INTO `tb_follow` VALUES (25, 9, 4, '2022-11-09 17:37:39');
INSERT INTO `tb_follow` VALUES (26, 9, 5, '2022-11-09 17:37:44');
INSERT INTO `tb_follow` VALUES (27, 10, 1, '2022-11-09 17:38:01');
INSERT INTO `tb_follow` VALUES (28, 10, 3, '2022-11-09 17:38:05');
INSERT INTO `tb_follow` VALUES (29, 10, 2, '2022-11-09 17:38:12');
INSERT INTO `tb_follow` VALUES (30, 10, 9, '2022-11-09 17:38:17');
INSERT INTO `tb_follow` VALUES (31, 10, 8, '2022-11-09 17:38:24');

-- ----------------------------
-- Table structure for tb_play
-- ----------------------------
DROP TABLE IF EXISTS `tb_play`;
CREATE TABLE `tb_play`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动名称',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '活动创建的userId',
  `type_id` bigint(20) UNSIGNED NOT NULL COMMENT '活动类型的id',
  `images` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动图片，多个图片以\',\'隔开',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动详细地址',
  `x` double UNSIGNED NOT NULL COMMENT '经度',
  `y` double UNSIGNED NOT NULL COMMENT '维度',
  `avg_price` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '活动均价，取整数',
  `need_number` int(10) UNSIGNED ZEROFILL NOT NULL COMMENT '活动需要的人',
  `content` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '活动的文字描述',
  `begin_time` date NOT NULL COMMENT '活动开始时间',
  `end_time` date NOT NULL COMMENT '活动结束时间',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_play
-- ----------------------------
INSERT INTO `tb_play` VALUES (70, '一起来看战狼1', 1, 2, '/images/play/2022/12/02/1/c9528cb2ae8145ed886d54c25d22f860.jpg,/images/play/2022/12/02/1/e1d22c214f7a452eb7a7528736c8152f.jpg,/images/play/2022/12/02/1/82af0b6788ac498bb3e6bd09e2ea97dc.jpg,/images/play/2022/12/02/1/78ca87c339684cbcbaf81f0ce277e2ac.jpg,/images/play/2022/12/02/1/fb6b1ddc9131461584963c30224a6d13.jpg', '广东省东莞市中山中路南铭购物广场三楼', 113.970158, 23.015442, 56, 0000000003, '爱国，爱国，爱国，爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国', '2022-11-01', '2022-11-01', '2022-12-02 14:07:39', '2022-12-02 23:08:22');
INSERT INTO `tb_play` VALUES (71, '一起来看红海行动', 1, 2, '/images/play/2022/12/02/1/575d4d8a98f940a3918d10645750dc56.jpg,/images/play/2022/12/02/1/b6b2a69e62204fda97d5f8b16851d28a.jpg,/images/play/2022/12/02/1/eaee3ad6f0854d29a59896090479f8cd.jpg,/images/play/2022/12/02/1/54e3eb6edfd14a34ab6d463ad3637aae.jpg', '广东省东莞市大岭山镇教育路新晖大厦(近广场路)天和百货(大岭山店)F5', 113.84045, 22.901289, 54, 0000000003, '红海行动，更爱国更爱国，爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国爱国', '2022-11-03', '2022-12-03', '2022-12-02 20:07:54', '2022-12-02 23:08:25');
INSERT INTO `tb_play` VALUES (72, '再来看一次战狼二', 1, 2, '/images/play/2022/12/02/1/f374620ba9a84245babdfb6cc44550b8.jpg,/images/play/2022/12/02/1/81595f79a42d4bf1a70cf9a684e23f7b.jpg,/images/play/2022/12/02/1/bea69c1810734df6a5cfa0bd45c74cc4.jpg,/images/play/2022/12/02/1/1c161f506a484ca99ee176110a1dd490.jpg', '广东省东莞市公益街清溪文化广场右翼1层', 114.16518, 22.835319, 45, 0000000003, '求你了，爱国就来看战狼二，不看不是中国人', '2022-10-04', '2022-10-04', '2022-12-02 20:09:35', '2022-12-02 23:08:27');
INSERT INTO `tb_play` VALUES (73, '一起来看漫展', 1, 1, '/images/play/2022/12/02/1/abbc41b98c174515b4421bccb894a516.jpg,/images/play/2022/12/02/1/349d59f42bc142f49b4182713e289bf7.jpg,/images/play/2022/12/02/1/8c0de4517e8349c090827ac3f8cb7e8c.jpg,/images/play/2022/12/02/1/cc7b907b2cc643fe83903cd6bfe2aafa.jpg', '广东省东莞市南城街道体育路3号', 113.748325, 23.024606, 210, 0000000002, '小哥哥，小姐姐，嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿嘿', '2022-12-02', '2022-12-03', '2022-12-02 20:11:27', '2022-12-02 20:11:27');
INSERT INTO `tb_play` VALUES (74, '游玩日月贝', 11, 1, '/images/play/2022/12/02/11/035ac5b978394aecb66b1e1078f8b8c8.jpg,/images/play/2022/12/02/11/77b5e493939c4b8a9eefdc18b48b2b2e.jpg,/images/play/2022/12/02/11/9ec7afdd1ae841fa8441043933e62b74.jpg,/images/play/2022/12/02/11/be8ddfb64f374a6fab919e659b500597.jpg,/images/play/2022/12/02/11/06d229986185441ca9553f733fce9674.jpg', '广东省珠海市香洲区新月桥(珠海大剧院)', 113.589134, 22.284029, 50, 0000000004, '珠海的日月贝是珠海的大剧院，也是位于广东境内建立在海上的一座歌剧院。它是由一大一小两个“贝壳”构成整体的美丽动人的形象，也被称为“日月贝”。', '2022-12-02', '2022-12-03', '2022-12-02 21:51:04', '2022-12-02 23:07:32');
INSERT INTO `tb_play` VALUES (75, '游玩孙中山纪念馆', 11, 1, '/images/play/2022/12/02/11/5abedfc7d7194dff922063eff357c4f5.jpg,/images/play/2022/12/02/11/15ca99f01b9b421f9c840e9c00c005b4.jpg,/images/play/2022/12/02/11/2ac7ba89238b498181bcd85a01ae24aa.jpg,/images/play/2022/12/02/11/930ccd51c6784ecc8c7bd14f1c80e53c.jpg,/images/play/2022/12/02/11/18d7cdb629ef4e339ceea25b928bcdcf.jpg,/images/play/2022/12/02/11/ba6eb31aa9d448d29088277745412e6c.jpg,/images/play/2022/12/02/11/2190ecafed1b469d94956cc159b7e60c.jpg', '广东省中山市翠亨大道93号', 113.52831, 22.442244, 50, 0000000004, '孙中山故居纪念馆设有\"孙中山故居纪念馆\"、\"中山市民俗博物馆\"、\"中山市孙中山研究所\"三个机构，分为孙中山纪念展示区、翠亨民居展示区、农耕文化展示区、杨殷、陆皓东纪念展示区、非物质文化遗产展示区、其他展区6大区域组成。', '2022-12-02', '2022-12-03', '2022-12-02 21:52:20', '2022-12-02 23:07:28');
INSERT INTO `tb_play` VALUES (76, '游玩西岸博览会', 11, 1, '/images/play/2022/12/02/11/db6eeb0394a249c1bd8f9feb150b76a7.png,/images/play/2022/12/02/11/2b113832c7534615933d77acf22436e0.png,/images/play/2022/12/02/11/8baf4d67151b43bb8592741d102d07d9.png', '上海市上海市徐汇区龙腾大道2555号', 121.46206, 31.167744, 50, 0000000004, '本届西岸博览会汇聚来自 19 个国家、43 个城市逾百家顶尖画廊与机 构参展，呈现绘画、雕塑、摄影、装置、设计及数字艺术等多种媒介的精彩创作，为大家 带来一场世界级艺术盛宴。\n', '2022-12-02', '2022-12-03', '2022-12-02 21:53:47', '2022-12-02 23:07:24');
INSERT INTO `tb_play` VALUES (77, '游玩艺术博览会', 11, 1, '/images/play/2022/12/02/11/1c205108f9414f54885f0e68f19613f2.png,/images/play/2022/12/02/11/f10aefb183744cdf8b9e3f4e37bd0413.png,/images/play/2022/12/02/11/54901742015349638f7dac44ada70105.png,/images/play/2022/12/02/11/b153291c5cd541a3b99add2dec5740c4.png,/images/play/2022/12/02/11/1983cf5d430947c5bc29f95d04fd8846.png,/images/play/2022/12/02/11/531a3d6ad5dd45d181dff889095551c2.png', '上海市上海市浦东新区龙阳路2345号', 121.567085, 31.210592, 50, 0000000004, '本届art021上海廿一当代艺术博览会汇聚了来自23个国家、36个城市的134 家海内外顶尖画廊的近万件当代艺术佳作，其中25家一线画廊首次参展。', '2022-12-02', '2022-12-03', '2022-12-02 21:55:02', '2022-12-02 23:07:23');
INSERT INTO `tb_play` VALUES (78, '狼人杀', 1, 4, '/images/play/2022/12/03/1/c39a0f6bec994d65812628f3dcd34735.jpg,/images/play/2022/12/03/1/1a2d84c282194c18928ffca1a21291e7.jpg,/images/play/2022/12/03/1/d8bdaaf9253647cc9ce72a37d4793e08.jpg,/images/play/2022/12/03/1/bf6d800d8d5a4945b84a23f93864498d.jpg', '广东省东莞市古梅路', 113.57946761167936, 23.065186485143137, 21, 0000000006, '一起来玩快乐的狼人杀', '2022-12-03', '2022-12-03', '2022-12-03 13:53:59', '2022-12-03 13:53:59');
INSERT INTO `tb_play` VALUES (79, '一起来看长津湖', 13, 2, '/images/play/2022/12/06/13/305bd7b591f5412c9842da0304b380ae.png', '广东省江门市', 113.03231, 22.45877, 30, 0000000004, '1950年，中国人民志愿军部队与美军在朝鲜长津湖地区交战，中国人民志愿军第9兵团将美军1个多师分割包围于长津湖地区，歼敌1.3万余人，扭转了战场态势。', '2022-12-05', '2022-12-08', '2022-12-06 14:28:53', '2022-12-06 14:28:53');
INSERT INTO `tb_play` VALUES (80, '一起看漫展', 1, 1, '/images/play/2022/12/08/1/86d11d78019548abbe0c74fbd41fb74e.jpg,/images/play/2022/12/08/1/2748f0f6795d4c10a8e92e69251f40ec.jpg,/images/play/2022/12/08/1/d164f3bee5df4ac3a3a9a50ec019a0f9.jpg,/images/play/2022/12/08/1/8a12e993d30a48e4b4ed88b3f2e7cf87.jpg', '广东省东莞市古梅路', 113.5791521568011, 23.065209243466782, 61, 0000000002, '好玩的漫展', '2022-12-07', '2022-12-10', '2022-12-08 10:32:31', '2022-12-08 10:36:04');

-- ----------------------------
-- Table structure for tb_play_type
-- ----------------------------
DROP TABLE IF EXISTS `tb_play_type`;
CREATE TABLE `tb_play_type`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '顺序',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_play_type
-- ----------------------------
INSERT INTO `tb_play_type` VALUES (1, '艺术展览', '/imgs/type/type01.jpg', 1, '2022-11-08 20:06:13', '2022-11-08 20:06:13');
INSERT INTO `tb_play_type` VALUES (2, '电影', '/imgs/type/type02.jpg', 2, '2022-11-08 20:06:46', '2022-11-08 20:06:46');
INSERT INTO `tb_play_type` VALUES (3, '游乐园', '/imgs/type/type03.jpg', 3, '2022-11-08 20:06:55', '2022-11-08 20:07:44');
INSERT INTO `tb_play_type` VALUES (4, '狼人杀', '/imgs/type/type04.jpg', 4, '2022-11-08 20:07:02', '2022-11-08 20:07:46');
INSERT INTO `tb_play_type` VALUES (5, '茶话会', '/imgs/type/type05.jpg', 5, '2022-11-08 20:07:09', '2022-11-08 20:07:54');
INSERT INTO `tb_play_type` VALUES (6, '棋牌', '/imgs/type/type06.jpg', 6, '2022-11-08 20:07:23', '2022-11-08 20:07:57');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `nick_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称，默认是用户id',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '人物头像',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户在小程序唯一标识符',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户在微信开放平台唯一标识符',
  `session_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录会话密钥',
  `customer_tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '判断是否为注册过的新老用户',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniqe_key_phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '15118626877', '缘昔', '/images/user/default.jpg', '2022-11-08 20:05:13', '2022-11-15 21:05:54', 'o79cX5KA4J8y5Y_JPuEhbHLaqR_4', NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (2, '13512950375', '宋炮对右', '/images/user/default.jpg', '2022-11-09 16:10:37', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (3, '13829472394', '东莞梦遗', '/images/user/py.jpg', '2022-11-09 16:11:03', '2022-12-01 16:23:36', 'o79cX5F2rrrRpeTkTnBdM3g4FZCU', NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (4, '13342850485', '物故韶钕', '/images/user/default.jpg', '2022-11-09 16:11:50', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (5, '18829472894', '刘烈柱', '/images/user/default.jpg', '2022-11-09 16:12:44', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (6, '13148343045', '藏花阿七', '/images/user/default.jpg', '2022-11-09 16:13:08', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (7, '19834863024', '青春猪头少年', '/images/user/default.jpg', '2022-11-09 16:13:37', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (8, '13455355664', '花开富贵', '/images/user/default.jpg', '2022-11-09 16:14:02', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (9, '15618734563', '一帆风顺', '/images/user/default.jpg', '2022-11-09 16:14:20', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (10, '14667433567', '添柴少年', '/images/user/default.jpg', '2022-11-09 16:15:05', '2022-12-03 10:48:46', NULL, NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (11, '13794174678', '终极悍匪ck', '/images/user/ck.jpg', '2022-12-01 15:01:00', '2022-12-01 15:02:46', 'o79cX5I8uQiD2a2lHK8GUleZB1xU', NULL, NULL, NULL);
INSERT INTO `tb_user` VALUES (13, '17328124160', 'Kyouko', '/images/user/djh.jpg', '2022-12-03 14:03:20', '2022-12-05 20:57:19', 'o79cX5HXf5FiT1AbPvvS-9TNbh2I', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_info`;
CREATE TABLE `tb_user_info`  (
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '主键，用户id',
  `city` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '城市名称',
  `content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '个人介绍，不要超过128个字符',
  `fans` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '粉丝数量',
  `followee` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '关注的人的数量',
  `gender` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '性别，0：男，1：女',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `credits` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '积分',
  `level` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '会员级别，0~9级,0代表未开通会员',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------
INSERT INTO `tb_user_info` VALUES (1, '', NULL, 0, 0, 0, NULL, 0, 0, '2022-11-30 11:38:31', '2022-11-30 11:38:31');

-- ----------------------------
-- Table structure for tb_want_player
-- ----------------------------
DROP TABLE IF EXISTS `tb_want_player`;
CREATE TABLE `tb_want_player`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '报名人的用户id',
  `play_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'playId',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1，申请中; 2,申请通过; 3,禁止参与',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '活动创建人的id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_want_player
-- ----------------------------
INSERT INTO `tb_want_player` VALUES (18, 2, 70, 1, 1);
INSERT INTO `tb_want_player` VALUES (19, 3, 70, 2, 1);
INSERT INTO `tb_want_player` VALUES (20, 4, 70, 2, 1);
INSERT INTO `tb_want_player` VALUES (21, 2, 71, 3, 1);
INSERT INTO `tb_want_player` VALUES (22, 3, 73, 1, 1);
INSERT INTO `tb_want_player` VALUES (23, 1, 77, 1, 11);
INSERT INTO `tb_want_player` VALUES (24, 1, 76, 1, 11);
INSERT INTO `tb_want_player` VALUES (25, 1, 75, 1, 11);
INSERT INTO `tb_want_player` VALUES (26, 1, 74, 1, 11);
INSERT INTO `tb_want_player` VALUES (27, 11, 70, 1, 1);
INSERT INTO `tb_want_player` VALUES (28, 11, 71, 1, 1);
INSERT INTO `tb_want_player` VALUES (29, 11, 72, 1, 1);
INSERT INTO `tb_want_player` VALUES (30, 13, 70, 2, 1);
INSERT INTO `tb_want_player` VALUES (31, 13, 71, 2, 1);
INSERT INTO `tb_want_player` VALUES (32, 1, 79, 1, 13);
INSERT INTO `tb_want_player` VALUES (33, 2, 79, 1, 13);
INSERT INTO `tb_want_player` VALUES (34, 3, 79, 2, 13);
INSERT INTO `tb_want_player` VALUES (35, 11, 80, 2, 1);
INSERT INTO `tb_want_player` VALUES (36, 3, 80, 2, 1);

SET FOREIGN_KEY_CHECKS = 1;
