package com.yurrize.service;


import com.yurrize.dto.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface FileUploadService {
    /**
     * 上传
     */
    String uploadFile(MultipartFile file, HttpServletRequest request, String pathName);


}
