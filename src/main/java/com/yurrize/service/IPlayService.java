package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.PlayDto;
import com.yurrize.dto.Result;
import com.yurrize.entity.Play;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface IPlayService extends IService<Play> {
    Result queryHotPlay(Integer current);

    Result queryByType(Integer typeId, Integer current);

    Result queryByUserId(Integer userId, Integer current);

    Result savePlay(PlayDto playDto);

    Result queryById(Long id);

    Result delete(Long id);

    Result uploadPlay(MultipartFile file, HttpServletRequest request) throws InterruptedException;

    Result queryMyPlay();
}
