package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.Result;
import com.yurrize.dto.WantPlayDto;
import com.yurrize.entity.WantPlayer;

public interface IWantPlayService extends IService<WantPlayer> {
    Result joinPlay(WantPlayer wantPlayer);

    Result managementMyPlay(Long id);


    Result queryByUserId();


    Result changeStatus(WantPlayer wantPlayer);
}
