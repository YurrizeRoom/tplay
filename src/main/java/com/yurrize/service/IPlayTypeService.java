package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.Result;
import com.yurrize.entity.PlayType;

public interface IPlayTypeService extends IService<PlayType> {
    Result queryType();

}
