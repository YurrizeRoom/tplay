package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.Result;
import com.yurrize.entity.UserInfo;


public interface IUserInfoService extends IService<UserInfo> {

    Result addInfo(UserInfo userInfo);

    Result queryMe();
}
