package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.entity.BlogComments;

public interface IBlogCommentsService extends IService<BlogComments> {
}
