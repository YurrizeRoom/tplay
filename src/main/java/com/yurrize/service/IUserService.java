package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.BindPhoneFormDto;
import com.yurrize.dto.Result;
import com.yurrize.entity.User;

import javax.servlet.http.HttpSession;

public interface IUserService extends IService<User> {

    Result login(String code);

    Result sendCode(String phone, HttpSession session);

    Result bandCode(BindPhoneFormDto phoneFormDto);

    Result getMe();

    Result querySimple();
}
