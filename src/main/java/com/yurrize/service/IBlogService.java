package com.yurrize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yurrize.dto.BlogDto;
import com.yurrize.dto.Result;
import com.yurrize.entity.Blog;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface IBlogService extends IService<Blog> {
    Result queryMyBlog(Integer current);

    Result queryBlogById(Long id);

    Result queryHotBlog(Integer current);

    Result queryBlogByUserId(Integer current, Long userId);

    Result saveBlog(BlogDto blogDto);

    Result likeBlog(Long blogId);

    Result uploadBlog(MultipartFile file, HttpServletRequest request);
}
