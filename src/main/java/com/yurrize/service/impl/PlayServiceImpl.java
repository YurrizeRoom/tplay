package com.yurrize.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.dto.PlayDto;
import com.yurrize.dto.Result;
import com.yurrize.dto.UserDTO;
import com.yurrize.entity.Play;
import com.yurrize.entity.User;
import com.yurrize.mapper.PlayMapper;
import com.yurrize.service.FileUploadService;
import com.yurrize.service.IPlayService;
import com.yurrize.service.IPlayTypeService;
import com.yurrize.service.IUserService;
import com.yurrize.utils.UserHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.yurrize.utils.RedisConstants.CACHE_IMAGE_KEY;
import static com.yurrize.utils.RedisConstants.CACHE_IMAGE_TTL;
import static com.yurrize.utils.SystemConstants.DEFAULT_PAGE_SIZE;
import static com.yurrize.utils.SystemConstants.MAX_PAGE_SIZE;

@Service
public class PlayServiceImpl extends ServiceImpl<PlayMapper, Play> implements IPlayService {

    @Resource
    private IUserService userService;

    @Resource
    private IPlayTypeService playTypeService;

    @Resource
    private FileUploadService fileUploadService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryHotPlay(Integer current) {
        // 根据用户排序
        Page<Play> page = query()
                .orderByDesc("create_time")
                .page(new Page<>(current, MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Play> records = page.getRecords();

        for (Play record : records) {
            if (record.getImages()==null){
                break;
            }
            List<String> collect = Arrays.stream(record.getImages().split(",")).collect(Collectors.toList());
            record.setImgList(collect);
        }
        return Result.ok(records);
    }

    @Override
    public Result queryByType(Integer typeId, Integer current) {


        if (typeId==null) {
            return Result.fail("没选择活动类型");
        }

        boolean flag = playTypeService.query().eq("id", typeId).exists();
        if (!flag){
            return Result.fail("不存在该数据");
        }
        Page<Play> page =query()
                .eq("type_id", typeId)
                .page(new Page<>(current, DEFAULT_PAGE_SIZE));
        List<Play> records = page.getRecords();
        for (Play record : records) {
            if (record.getImages()==null){
                break;
            }
            List<String> collect = Arrays.stream(record.getImages().split(",")).collect(Collectors.toList());
            record.setImgList(collect);
        }
        //返回数据
        return Result.ok(records);
    }

    @Override
    public Result queryByUserId(Integer userId, Integer current) {

        boolean flag = query().eq("user_id", userId).exists();
        if (!flag){
            return Result.fail("不存在该数据");
        }

        Page<Play> page = query()
                .eq("user_id", userId)
                .page(new Page<>(current, DEFAULT_PAGE_SIZE));
        List<Play> records = page.getRecords();
        for (Play record : records) {
            if (record.getImages()==null){
                break;
            }
            List<String> collect = Arrays.stream(record.getImages().split(",")).collect(Collectors.toList());
            record.setImgList(collect);
        }
        //返回数据
        return Result.ok(records);
    }

    @Override
    public  Result savePlay(PlayDto playDto) {


        //获取当前登录账户
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先登录");
        }


        Long typeId = playDto.getTypeId();
        if (typeId==null) {
            return Result.fail("请选择活动类型");
        }

        //查询传进来的活动类型是否正确 SELECT EXISTS(SELECT 1 FROM tb_play_type where id=7);
        boolean flag = playTypeService.query().eq("id", typeId).exists();
        if (!flag){
            return Result.fail("不存在该活动类型");
        }
        List<String> imgs = playDto.getImgs();
        String collect = imgs.stream().map(String::valueOf).collect(Collectors.joining(","));

//        String url = imgList.stream().map(String::valueOf).collect(Collectors.joining(","));
//        String url = stringRedisTemplate.opsForList().rightPop(CACHE_IMAGE_KEY + user.getId() + ":play");
//
        Play play = BeanUtil.copyProperties(playDto, Play.class);
        play.setUserId(user.getId());
//
//        if (url!=null) {
//
//            //拿到图片路径byID
//            ArrayList<String> urls = new ArrayList<>();
//
//            while (url!=null){
//                urls.add(url);
//                url=stringRedisTemplate.opsForList().rightPop(CACHE_IMAGE_KEY + user.getId() + ":play");
//            }
//
//            String collect = String.join(",", urls);
//            play.setImages(collect);
//        }

        play.setImages(collect);
        save(play);
        return Result.ok("创建成功");


    }

    @Override
    public synchronized  Result uploadPlay(MultipartFile file, HttpServletRequest request) {
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            return Result.fail("请先登录");
        }
        String pathName="play/";

        String url = fileUploadService.uploadFile(file, request, pathName);


        if (url==null||url.isEmpty()){
            return Result.fail("上传失败");
        }

        stringRedisTemplate.opsForList().leftPush(CACHE_IMAGE_KEY+user.getId()+":play",url);

        stringRedisTemplate.expire(CACHE_IMAGE_KEY+user.getId()+":play",CACHE_IMAGE_TTL, TimeUnit.MINUTES);


        return Result.ok(url);
    }

    @Override
    public Result queryMyPlay() {
        // 获取登录用户
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先登录");
        }
        // 根据用户查询
        List<Play> records = query().eq("user_id", user.getId()).list();
        // 获取当前页数据
        for (Play record : records) {
            if (record.getImages()==null){
                break;
            }
            List<String> collect = Arrays.stream(record.getImages().split(",")).collect(Collectors.toList());
            record.setImgList(collect);
        }
        return Result.ok(records);
    }

    //TODO 将活动信息保存到redis降低mysql负担

    @Override
    public Result queryById(Long id) {

        Play play = getById(id);
        if (play==null){
            return Result.fail("活动不存在");
        }
        if (play.getImages()==null) {
            queryPlayUser(play);
            return Result.ok(play);
        }
        List<String> collect = Arrays.stream(play.getImages().split(",")).collect(Collectors.toList());
        play.setImgList(collect);
        queryPlayUser(play);
        return Result.ok(play);
    }

    @Override
    public Result delete(Long id) {

        //获取当前登录账户
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            return Result.fail("请先登录");
        }

        boolean flag = query().eq("id", id).exists();
        if (!flag){
            return Result.fail("不存在该活动，无法删除");
        }

        boolean result = removeById(id);
        return Result.ok("删除playId为"+id+"结果为:"+result);
    }

    private void queryPlayUser(Play play) {
        Long userId = play.getUserId();
        User user = userService.getById(userId);
        play.setUserName(user.getNickName());
        play.setIcon(user.getIcon());
    }


}
