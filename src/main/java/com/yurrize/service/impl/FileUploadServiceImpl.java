package com.yurrize.service.impl;

import com.yurrize.service.FileUploadService;
import com.yurrize.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
public class FileUploadServiceImpl implements FileUploadService {

    /**
     * 时间格式化
     */
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");


    @Value("${file-save-path}")
    private String fileSavePath;

    public Integer id=1;

    @Override
    public  String uploadFile(MultipartFile file, HttpServletRequest request, String pathName) {

        //1.后半段目录
        String directory = simpleDateFormat.format(new Date())+ UserHolder.getUser().getId().toString()+"/";

        /**
         *  2.文件保存目录  E:/images/play/2020/03/15/
         *  如果目录不存在，则创建
         */

        String realPath=fileSavePath+pathName+directory;

        File dir = new File(realPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        log.info("图片上传，保存位置：" + realPath);

        //3.给文件重新设置一个名字
        //后缀
        String suffix = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));


        String newFileName= UUID.randomUUID().toString().replaceAll("-", "")+suffix;

//        String newFileName= LocalDateTime.now().toString().replace("-","") +suffix;

        //4.创建这个新文件
        File newFile = new File(realPath + newFileName);

        //5.复制操作
        try {
            file.transferTo(newFile);
            //协议 :// ip地址 ：端口号 / 文件目录(/images/2020/03/15/xxx.jpg)
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/images/" + pathName + directory + newFileName;
            log.info("图片上传，访问URL：" + url);

            return "/images/" + pathName + directory + newFileName;
        } catch (IOException e) {

            return null;


        }

    }




}
