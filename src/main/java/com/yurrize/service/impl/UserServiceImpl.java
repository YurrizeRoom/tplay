package com.yurrize.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.RandomUtil;

import com.yurrize.dto.WantPlayDto;
import com.yurrize.entity.UserInfo;
import com.yurrize.service.IUserInfoService;
import net.sf.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.yurrize.dto.BindPhoneFormDto;
import com.yurrize.dto.Result;

import com.yurrize.dto.UserDTO;
import com.yurrize.entity.User;
import com.yurrize.mapper.UserMapper;
import com.yurrize.service.IUserService;
import com.yurrize.utils.CommonUtils;
import com.yurrize.utils.RegexUtils;

import com.yurrize.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

import javax.servlet.http.HttpSession;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.yurrize.utils.RedisConstants.*;
import static com.yurrize.utils.SystemConstants.USER_ICON_PREFIX;
import static com.yurrize.utils.SystemConstants.USER_NICK_NAME_PREFIX;


@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private UserMapper userMapper;

    @Override
    public Result login(String code) {


        if (code==null){
            return Result.fail("请先登录");
        }

        String url = new CommonUtils().appendUrl(code).toString();

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);

        if (response==null){
            return Result.fail("请求超时，请重试");
        }


        JSONObject jsonObj = JSONObject.fromObject(response);
        System.out.println(jsonObj);
        if (jsonObj.isEmpty()){
            return Result.fail("请求失败");
        }

        if (jsonObj.get("openid") == null) {
            return Result.fail("openId为空");
        }


        String openid = jsonObj.get("openid").toString();
        String session_key = jsonObj.get("session_key").toString();

//        String[] strings = response.split(",");
//        String openid = strings[1].substring(10, strings[1].length()-2);
//        String session_key = strings[0].substring(16, strings[0].length()-1);

        User user = query().eq("openid", openid).one();

        if (user==null){
            //如果为空，代表没注册，先进行注册
            user=createUserWithOpenId(openid);
        }



        //用户存在，进行登录操作，存入redis，设置ttl
        //token作为登录令牌
        String tokenKey=LOGIN_USER_KEY+openid;


        //判断是否需要命中数据库
        Boolean isLogin = stringRedisTemplate.hasKey(tokenKey);
        if (Boolean.TRUE.equals(isLogin)){
            return Result.ok(openid);
        }

        //user对象转换成hashmap
        UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
        Map<String, Object> userMap = BeanUtil.beanToMap(userDTO,new HashMap<>(),
                CopyOptions.create().setIgnoreNullValue(true).setFieldValueEditor((fieldName, fieldValue)->fieldValue.toString()));
        userMap.put("sessionKey",session_key);

        stringRedisTemplate.opsForHash().putAll(tokenKey,userMap);
        stringRedisTemplate.expire(tokenKey,LOGIN_USER_TTL,TimeUnit.MINUTES);

        return Result.ok(openid);
    }

    @Override
    //绑定手机号
    public Result sendCode(String phone, HttpSession session) {
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先进行登录");
        }

        //1.校验手机号
        if (RegexUtils.isPhoneInvalid(phone)) {
            //2.如果不符合返回错误信息
            return Result.fail("手机号格式错误，请重新输入");
        }

        //3.符合生成验证码
        String code = RandomUtil.randomNumbers(6);

        //保存到redis中
        stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY,code,LOGIN_CODE_TTL, TimeUnit.MINUTES);
        stringRedisTemplate.opsForValue().set(LOGIN_PHONE_KEY,phone,LOGIN_CODE_TTL, TimeUnit.MINUTES);


        //发送验证码,没钱做
        log.debug("发送短信验证码成功，验证码为:{}",code);
        return Result.ok("验证码发送成功，验证码为"+code);
    }

    @Override
    public Result bandCode(BindPhoneFormDto phoneFormDto) {

        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先进行登录");
        }

        if (phoneFormDto==null){
            return Result.fail("请输入正确的手机号码或者验证码");
        }

        //从前端获取到的表单手机号码
        String phone = phoneFormDto.getPhone();

        //从redis中拿手机号进行比对，看是否手机号一致
        String cachePhone = stringRedisTemplate.opsForValue().get(LOGIN_PHONE_KEY);
        if (cachePhone==null||!cachePhone.equals(phone)){
            //手机号输入错误报错
            return Result.fail("手机号输入不一致");
        }

        //从前端输入进来的code
        String code = phoneFormDto.getCode();

        //从redis拿到的code
        String  cacheCode= stringRedisTemplate.opsForValue().get(LOGIN_CODE_KEY);
        //数据校验
        if (cacheCode == null||!cacheCode.equals(code)) {
            //3.不一致报错
            return Result.fail("验证码错误");
        }

        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",user.getId()).set("phone",phone);
        boolean update = update(updateWrapper);
        if (!update){
            return Result.fail("服务器异常，绑定手机号失败");
        }
        return Result.ok(true);

    }

    //返回用户信息
    @Override
    public Result getMe() {
        //获取当前登录的用户并返回
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("当前无用户登录信息");
        }

        UserInfo userInfo = new UserInfo();


        return Result.ok(user);
    }

    @Override
    public Result querySimple() {
        UserDTO user = UserHolder.getUser();
        if (user == null) {
            return Result.fail("请先登录");
        }

        List<WantPlayDto> list = userMapper.queryUserSimple(user.getId());
        return Result.ok(list);
    }

    //根据用户openId创建用户
    private User createUserWithOpenId(String openId) {
        User user = new User();
        user.setOpenid(openId);
        user.setNickName(USER_NICK_NAME_PREFIX+RandomUtil.randomString(10));
        user.setIcon(USER_ICON_PREFIX);
        save(user);
        return user;
    }

}
