package com.yurrize.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.dto.BlogDto;
import com.yurrize.dto.Result;
import com.yurrize.dto.UserDTO;
import com.yurrize.entity.Blog;
import com.yurrize.entity.User;
import com.yurrize.mapper.BlogMapper;
import com.yurrize.service.FileUploadService;
import com.yurrize.service.IBlogService;
import com.yurrize.service.IPlayService;
import com.yurrize.service.IUserService;
import com.yurrize.utils.UserHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.yurrize.utils.RedisConstants.*;
import static com.yurrize.utils.SystemConstants.MAX_PAGE_SIZE;

@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog>implements IBlogService {


    @Resource
    private IUserService userService;

    @Resource
    private IPlayService playService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private FileUploadService fileUploadService;

    /**
     * 查询自己账号的blog
     * @param current 分页
     * @return records
     */
    @Override
    public Result queryMyBlog(Integer current) {

        // 获取登录用户
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            return Result.fail("请先登录");
        }
        // 根据用户查询
        Page<Blog> page = query()
                .eq("user_id", user.getId()).page(new Page<>(current, MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Blog> records = page.getRecords();
        return Result.ok(records);
    }

    /**
     * 根据blogId查询blog
     * @param id 用户id
     * @return blog
     */
    @Override
    public Result queryBlogById(Long id) {
        //1.查询blog
        Blog blog = getById(id);
        if (blog==null){
            return Result.fail("笔记不存在");
        }
        //2.查询blog有关的用户
        queryBlogUser(blog);
        //3.查询blog是否被点赞
        isBlogLiked(blog);
        return Result.ok(blog);
    }

    /**
     * 查询是否被点赞
     * @param blog blog
     */
    private void isBlogLiked(Blog blog) {
        //1.获取用户登录
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            //用户未登录
            return;
        }
        Long userId = user.getId();

        //2.判断当前用户是否已经点赞
        String key="blog:liked:"+blog.getId();
        Double score=stringRedisTemplate.opsForZSet().score(key, userId.toString());
        blog.setIsLike(score!=null);
    }

    /**
     * 点赞blog
     * @param blogId blogId
     * @return msg:ok
     */
    @Override
    public Result likeBlog(Long blogId) {
        //1.获取用户登录
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            //用户未登录
            return Result.fail("用户未登录");
        }
        Long userId = UserHolder.getUser().getId();

        //2.判断当前用户是否已经点赞
        String key=BLOG_LIKED_KEY+blogId;
        Double score = stringRedisTemplate.opsForZSet().score(key, userId.toString());
        if (score==null) {
            //3.如果为点赞可以点赞
            //3.1 数据库点赞数+1
            boolean isSuccess = update().setSql("liked=liked+1").eq("id", blogId).update();

            //3.2保存用户到Redis的set集合
            if (isSuccess){
                stringRedisTemplate.opsForZSet().add(key,userId.toString(),System.currentTimeMillis());
            }
        }else {
            //4.如果已点赞，取消点赞
            //4.1 数据库点赞数-1
            boolean isSuccess = update().setSql("liked=liked-1").eq("id", blogId).update();
            //4.2用户从Redis的set集合移除
            if (isSuccess) {
                stringRedisTemplate.opsForZSet().remove(key, userId.toString());
            }
        }
        return Result.ok();
    }

    /**
     * 查询热门Blog
     * @param current 分页数据
     * @return blog
     */
    @Override
    public Result queryHotBlog(Integer current) {
        // 根据用户查询
        Page<Blog> page = query()
                .orderByDesc("liked")
                .page(new Page<>(current, MAX_PAGE_SIZE));
        // 获取当前页数据
        List<Blog> records = page.getRecords();
        // 查询用户
        records.forEach(blog -> {
            this.queryBlogUser(blog);
            this.isBlogLiked(blog);
        });
        return Result.ok(records);
    }

    /**
     * 根据userId查询blog
     * @param current 分页数据
     * @param userId userId，用户id
     * @return blogList
     */
    @Override
    public Result queryBlogByUserId(Integer current, Long userId) {
        Page<Blog> page = query()
                .eq("user_id", userId).page(new Page<>(current, MAX_PAGE_SIZE));

        List<Blog> records = page.getRecords();
        return Result.ok(records);
    }

    /**
     * 创建blog
     * @param blogDto 封装blog
     * @return msg：success
     */
    @Override
    public Result saveBlog(BlogDto blogDto) {
        //获取当前登录账户
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先登录");
        }
        Long playId = blogDto.getPlayId();
        if (playId==null){
            return Result.fail("请选择活动");
        }
        boolean flag = playService.query().eq("id", playId).exists();
        if (!flag){
            return Result.fail("不存在该活动");
        }

        //拿到图片路径byID
        ArrayList<String> urls = new ArrayList<>();

        String url = stringRedisTemplate.opsForList().rightPop(CACHE_IMAGE_KEY + user.getId() + ":blog");
        while (url!=null){
            urls.add(url);
            url=stringRedisTemplate.opsForList().rightPop(CACHE_IMAGE_KEY + user.getId() + ":blog");
        }

        String collect = String.join(",", urls);
        Blog blog = BeanUtil.copyProperties(blogDto, Blog.class);
        blog.setUserId(user.getId());
        blog.setImages(collect);

        save(blog);


        return Result.ok("创建成功");
    }

    /**
     * 上传blog图片
     * @param file 文件
     * @param request 请求
     * @return ok
     */
    @Override
    public Result uploadBlog(MultipartFile file, HttpServletRequest request) {
        UserDTO user = UserHolder.getUser();
        if (user==null) {
            return Result.fail("请先登录");
        }
        String pathName="blog/";
        String url = fileUploadService.uploadFile(file, request, pathName);

        if (url==null||url.isEmpty()){
            return Result.fail("上传失败");
        }

        stringRedisTemplate.opsForList().leftPush(CACHE_IMAGE_KEY+user.getId()+":blog",url);
        stringRedisTemplate.expire(CACHE_IMAGE_KEY+user.getId()+":blog",CACHE_IMAGE_TTL, TimeUnit.MINUTES);

        return Result.ok("上传成功");
    }

    /**
     * 根据blog查询用户对应信息
     * @param blog blog信息
     */
    private void queryBlogUser(Blog blog) {
        //2.查询blog有关的用户
        Long userId = blog.getUserId();
        User user = userService.getById(userId);
        blog.setName(user.getNickName());
        blog.setIcon(user.getIcon());
    }


}
