package com.yurrize.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.entity.BlogComments;
import com.yurrize.mapper.BlogCommentsMapper;
import com.yurrize.service.IBlogCommentsService;
import org.springframework.stereotype.Service;

@Service
public class BlogCommentsServiceImpl extends ServiceImpl<BlogCommentsMapper, BlogComments> implements IBlogCommentsService {
}
