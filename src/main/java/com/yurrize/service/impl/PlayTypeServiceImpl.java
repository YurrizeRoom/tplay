package com.yurrize.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.dto.Result;
import com.yurrize.entity.PlayType;
import com.yurrize.mapper.PlayTypeMapper;
import com.yurrize.service.IPlayTypeService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.yurrize.utils.RedisConstants.CACHE_TYPE_KEY;
import static com.yurrize.utils.RedisConstants.CACHE_TYPE_TTL;

@Service
public class PlayTypeServiceImpl extends ServiceImpl<PlayTypeMapper, PlayType>implements IPlayTypeService {


    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryType() {
        String key = CACHE_TYPE_KEY ;
        // 1.从redis中查询数据
        String typeJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (typeJson != null){
            // 3.存在，返回数据
            List<PlayType> shopTypeList = JSONUtil.toList(typeJson, PlayType.class);
            return Result.ok(shopTypeList);
        }
        // 4.不存在，查询数据库
        List<PlayType> list = query().orderByAsc("sort").list();
        // 5.不存在，返回错误
        if (list == null) {
            return Result.fail("分类不存在");
        }
        // 6.存在存入redis中
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(list),CACHE_TYPE_TTL, TimeUnit.MINUTES);
        // 7.返回
        return Result.ok(list);
    }
}
