package com.yurrize.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.dto.*;
import com.yurrize.entity.Play;
import com.yurrize.entity.User;
import com.yurrize.entity.WantPlayer;
import com.yurrize.mapper.WantPlayerMapper;
import com.yurrize.service.IPlayService;
import com.yurrize.service.IUserService;
import com.yurrize.service.IWantPlayService;

import com.yurrize.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class WantPlayServiceImpl extends ServiceImpl<WantPlayerMapper, WantPlayer>implements IWantPlayService {

    @Resource
    private IPlayService playService;

    @Resource
    private IUserService userService;

    @Resource
    WantPlayerMapper wantPlayerMapper;


    /**
     * 参加活动
     * @param wantPlayer pojo
     * @return result
     */
    @Override
    @Transactional
    public Result joinPlay(WantPlayer wantPlayer) {
        UserDTO user = UserHolder.getUser();

        if (user==null){
            return Result.fail("请先登录");
        }


        Play play = playService.getById(wantPlayer.getPlayId());

        if (play==null){
            return Result.fail("没有这个活动!");
        }

        wantPlayer.setUserId(user.getId());

        if (play.getUserId().equals(user.getId())) {

            return Result.fail("不能自己报名自己的活动哦！");
        }
        wantPlayer.setCreateId(play.getUserId());

        if (play.getNeedNumber()==null||play.getNeedNumber()==0) {
            return Result.fail("活动人满了");
        }
        if (query().eq("user_id", wantPlayer.getUserId()).eq("play_id",wantPlayer.getPlayId()).exists()){
            return Result.fail("已经报名成功，无需重复报名");
        }

        boolean success = playService.update()
                .setSql("need_number=need_number-1")
                .eq("id", wantPlayer.getPlayId())
                .gt("need_number", 0)
                .update();

        boolean save = save(wantPlayer);

        if (!success||!save) {
            //扣减失败
            log.error("人数达标");
            return Result.fail("活动人员满了");
        }



        return Result.ok("报名成功!");
    }

    /**
     * 查询参加我活动的人
     * @param id play 我创建的活动id
     * @return result
     */
    @Override
    public Result managementMyPlay(Long id) {

        UserDTO user = UserHolder.getUser();
        if (user == null) {
            return Result.fail("请先登录");
        }
        List<WantPlayDto> list = wantPlayerMapper.queryWant(id);
        return Result.ok(list);
    }


    @Override
    public Result queryByUserId() {

        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先登录");
        }
        List<WantPlayer> list = query().eq("user_id", user.getId()).list();
        if (list.isEmpty()){
            return Result.fail("查询错误");
        }
        return Result.ok(list);

    }

    @Override
    public Result changeStatus(WantPlayer wantPlayer) {
        UserDTO user = UserHolder.getUser();
        if (user==null){
            return Result.fail("请先登录");
        }
        boolean update = update().eq("play_id", wantPlayer.getPlayId()).eq("user_id", wantPlayer.getUserId()).set("status", wantPlayer.getStatus()).update();
        if (!update) {
            return Result.fail("修改失败");
        }
        return Result.ok("修改成功");
    }


}
