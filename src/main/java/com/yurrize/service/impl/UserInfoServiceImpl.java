package com.yurrize.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yurrize.dto.Result;
import com.yurrize.dto.UserDTO;
import com.yurrize.entity.UserInfo;
import com.yurrize.mapper.UserInfoMapper;
import com.yurrize.service.IUserInfoService;
import com.yurrize.utils.UserHolder;
import org.springframework.stereotype.Service;


@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    @Override
    public Result addInfo(UserInfo userInfo) {

        UserDTO user = UserHolder.getUser();
        if (user == null) {
            return Result.fail("请先登录");
        }

        Long id = user.getId();
        boolean flag = query().eq("user_id", id).exists();

        if (flag){
            return Result.ok();
        }

        userInfo.setUserId(user.getId());
        save(userInfo);
        return Result.ok("ok");
    }

    @Override
    public Result queryMe() {
        UserDTO user = UserHolder.getUser();
        if (user == null) {
            return Result.fail("请先登录");
        }
        Long id = user.getId();
        UserInfo me = getById(id);
        if (me==null){
            return Result.fail("不存在用户");
        }
        me.setIcon(user.getIcon());
        me.setNickName(user.getNickName());

        return Result.ok(me);
    }


}
