package com.yurrize.controller;

import com.yurrize.dto.BlogDto;
import com.yurrize.dto.Result;
import com.yurrize.service.IBlogService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/blog")
public class BlogController {

    @Resource
    private IBlogService blogService;

    @GetMapping("/me")
    public Result queryMyBlog(@RequestParam(value = "current",defaultValue = "1")Integer current){
        return blogService.queryMyBlog(current);
    }

    @GetMapping("/{id}")
    public Result queryBlogById(@PathVariable("id") Long id){
        return blogService.queryBlogById(id);
    }

    @GetMapping("/hot")
    public Result queryHotBlog(@RequestParam(value = "current", defaultValue = "1") Integer current) {
        return blogService.queryHotBlog(current);
    }

    @GetMapping("/user")
    public Result queryBlogByUserId(@RequestParam(value = "current",defaultValue = "1")Integer current,
                                    @RequestParam("id")Long userId){
        return blogService.queryBlogByUserId(current,userId);
    }

    //1.上传活动图片
    @PostMapping("/upload")
    public Result uploadPlay(MultipartFile file, HttpServletRequest request){
        return blogService.uploadBlog(file,request);
    }

    @PostMapping("/save")
    public Result saveBlog(@RequestBody BlogDto blogDto) {
        return blogService.saveBlog(blogDto);
    }



}
