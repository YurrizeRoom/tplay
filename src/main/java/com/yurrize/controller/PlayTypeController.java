package com.yurrize.controller;

import com.yurrize.dto.Result;
import com.yurrize.service.IPlayTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/type")
public class PlayTypeController {

    @Resource
    private IPlayTypeService playTypeService;

    @GetMapping("getType")
    public Result queryType(){
        return playTypeService.queryType();
    }
}
