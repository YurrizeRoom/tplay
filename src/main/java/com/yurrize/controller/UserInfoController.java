package com.yurrize.controller;

import com.yurrize.dto.Result;
import com.yurrize.entity.UserInfo;
import com.yurrize.service.IUserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/info")
public class UserInfoController {

    @Resource
    private IUserInfoService userInfoService;


    /**
     * 新增个人消息
     * @param userInfo 个人详细信息
     * @return 返回ok
     */
    @PostMapping("/add")
    public Result addInfo(@RequestBody UserInfo userInfo){
        return userInfoService.addInfo(userInfo);
    }

    @GetMapping("/me")
    public Result queryMySelf(){
        return userInfoService.queryMe();
    }


}
