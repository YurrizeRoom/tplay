package com.yurrize.controller;

import com.yurrize.dto.Result;
import com.yurrize.dto.WantPlayDto;
import com.yurrize.entity.WantPlayer;
import com.yurrize.service.IUserService;
import com.yurrize.service.IWantPlayService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/want")
public class WantPlayerController {

    @Resource
    private IWantPlayService wantPlayService;

    @Resource
    private IUserService userService;

    /**
     * 参加活动
     * @param wantPlayer 报名表
     * @return result
     */
    @PostMapping ("/join")
    public Result joinPlay(@RequestBody WantPlayer wantPlayer){
        return wantPlayService.joinPlay(wantPlayer);
    }

    /**
     * 查询参加我活动的人
     * @param id play 我创建的活动id
     * @return result
     */
    @GetMapping("/management")
    public Result managementMyPlay(@RequestParam("id")Long id){
        return wantPlayService.managementMyPlay(id);
    }


    /**
     * 所有我报名的活动的状态
     * @return msg
     */
    @GetMapping("/me")
    public Result wantMe(){
        return userService.querySimple();
    }



    @PostMapping("/changeStatus")
    public Result changeStatus(@RequestBody WantPlayer wantPlayer){
        return wantPlayService.changeStatus(wantPlayer);
    }


    /**
     * 查询自己申请活动的状态
     * @return
     */
    @GetMapping("/getStatus")
    public Result queryStatus(){
        return wantPlayService.queryByUserId();
    }


}
