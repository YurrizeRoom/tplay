package com.yurrize.controller;


import com.yurrize.dto.BindPhoneFormDto;
import com.yurrize.dto.Result;
import com.yurrize.dto.UserDTO;
import com.yurrize.service.IUserService;
import com.yurrize.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    /**
     * 登录功能
     * @param code wx.login传入来的code
     */
    @GetMapping("/login")
    public Result login(@RequestParam("code")String code){
        return userService.login(code);
    }

    /**
     * 绑定手机号，发送验证码
     */
    @PostMapping("/code")
    public Result sendCode(@RequestParam("phone") String phone, HttpSession session){
        return userService.sendCode(phone,session);
    }

    /**
     * 绑定手机号
     * @param  phoneForm dto类
     *
     */
    @PostMapping("/bind")
    public Result bindCode(@RequestBody BindPhoneFormDto phoneForm){
        return userService.bandCode(phoneForm);
    }

    /**
     * 获取当前用户登录信息并返回
     */
    @GetMapping("/me")
    public Result me(){
        return userService.getMe();
    }


}
