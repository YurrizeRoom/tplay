package com.yurrize.controller;


import com.yurrize.dto.PlayDto;
import com.yurrize.dto.Result;
import com.yurrize.service.IPlayService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/play")
public class PlayController {


    @Resource
    private IPlayService playService;


    //1.上传活动图片
    @PostMapping("/upload")
    public Result uploadPlay(MultipartFile file, HttpServletRequest request) throws InterruptedException {
        return playService.uploadPlay(file,request);
    }

    //2.新建活动
    @PostMapping("/save")
    public Result savePlay(@RequestBody PlayDto playDto){
        return playService.savePlay(playDto);
    }



    //TODO 将redis的url数据持久化
    //根据typeId查询活动
    @GetMapping("/of/type")
    public Result queryPlayByType(
            @RequestParam("typeId") Integer typeId,
            @RequestParam(value = "current",defaultValue = "1") Integer current
    ){
        return playService.queryByType(typeId,current);
    }

    //查询用户创建的活动
    @GetMapping("/of/user")
    public Result queryPlayByUserId(
            @RequestParam("userId") Integer userId,
            @RequestParam(value = "current",defaultValue = "1") Integer current
    ){
        return playService.queryByUserId(userId,current);
    }

    /**
     * 查询我创建的所有的活动
     * @return result
     */
    @GetMapping("/me")
    public Result queryMyPlay(){
        return playService.queryMyPlay();
    }

    //分页查询热门活动
    @GetMapping("/hot")
    public Result queryHotPlay(@RequestParam(value = "current", defaultValue = "1") Integer current) {
        return playService.queryHotPlay(current);
    }

    //根据playId查询play
    @GetMapping("/of/id")
    public Result queryPlayById(@RequestParam("id") Long id) {
        return playService.queryById(id);
    }

    //删除活动
    @DeleteMapping("/delete")
    public Result deletePlay(@RequestParam("id")Long id){
        return playService.delete(id);
    }


}
