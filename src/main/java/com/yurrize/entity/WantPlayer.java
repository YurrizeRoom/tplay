package com.yurrize.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yurrize.dto.Result;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * <p>
 * 
 * </p>
 *
 * @author 缘昔
 * @since 2022-11-8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_want_player")
public class WantPlayer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long Id;

    /**
     * 报名人用户id
     */
    private Long userId;

    /**
     * 活动创建人Id
     */
    private Long createId;


    /**
     * playId
     */
    private Long playId;



    /**
     * 申请状态 1，申请中; 2,申请通过; 3,禁止参与
     */
    private Integer status;

}
