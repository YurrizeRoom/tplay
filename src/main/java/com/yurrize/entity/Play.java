package com.yurrize.entity;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yurrize
 * @since 2022-11-8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_play")
public class Play implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 活动类型的id
     */
    private Long typeId;

    /**
     * 图片列表
     */
    @TableField(exist = false)
    private List<String> imgList;

    /**
     * 活动图片，多个图片以','隔开
     */
    private String images;

    /**
     * 活动地址
     */
    private String address;

    /**
     * 经度
     */
    private Double x;

    /**
     * 维度
     */
    private Double y;

    /**
     * 均价，取整数
     */
    private Long avgPrice;

    /**
     * 活动需要人数
     */
    private Integer needNumber;

    /**
     * 活动介绍
     */
    private String content;
    /**
     * 用户图标
     */
    @TableField(exist = false)
    private String icon;
    /**
     * 用户姓名
     */
    @TableField(exist = false)
    private String userName;
    /**
     * 生效时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= DatePattern.NORM_DATE_PATTERN)
    private Date beginTime;

    /**
     * 失效时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= DatePattern.NORM_DATE_PATTERN)
    private Date endTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    @TableField(exist = false)
    private Double distance;
}
