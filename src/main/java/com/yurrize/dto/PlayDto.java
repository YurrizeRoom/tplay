package com.yurrize.dto;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class PlayDto {
    /**
     * 活动名称
     */
    private String name;



    /**
     * 活动类型的id
     */
    private Long typeId;


    /**
     * 活动地址
     */
    private String address;

    /**
     * 经度
     */
    private Double x;

    /**
     * 维度
     */
    private Double y;

    /**
     * 均价，取整数
     */
    private Long avgPrice;

    /**
     * 活动需要人数
     */
    private Integer needNumber;

    /**
     * 活动介绍
     */
    private String content;

    private List<String> imgs;


    /**
     * 生效时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern=DatePattern.NORM_DATE_PATTERN)
    private Date beginTime;

    /**
     * 失效时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= DatePattern.NORM_DATE_PATTERN)
    private Date endTime;

}
