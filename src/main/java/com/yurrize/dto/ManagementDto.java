package com.yurrize.dto;

import lombok.Data;

@Data
public class ManagementDto {

    /**
     * 昵称，默认是随机字符
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String icon = "";


    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动需要人数
     */
    private Integer needNumber;

    /**
     * 申请状态 1，申请中; 2,申请通过; 3,禁止参与
     */
    private Integer status;

}
