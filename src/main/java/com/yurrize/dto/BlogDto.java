package com.yurrize.dto;

import lombok.Data;

@Data
public class BlogDto {
    /**
     * 活动id
     */
    private Long playId;
    /**
     * 标题
     */
    private String title;
    /**
     * 活动心得
     */
    private String content;


}
