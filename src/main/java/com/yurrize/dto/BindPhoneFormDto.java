package com.yurrize.dto;

import lombok.Data;

@Data
public class BindPhoneFormDto {
    private String phone;
    private String code;
}
