package com.yurrize.utils;

/**
 * 封装微信小程序appid, secret js_code  为获得用户openId
 */
public class CommonUtils {

    public StringBuffer appendUrl(String code) {

        StringBuffer info = new StringBuffer("https://api.weixin.qq.com/sns/jscode2session?");

        info.append("appid=").append("appid").append("&");

        info.append("secret=").append("secret").append("&");

        info.append("js_code=").append(code).append("&");

        info.append("grant_type=").append("authorization_code");

        return info;
    }
}
