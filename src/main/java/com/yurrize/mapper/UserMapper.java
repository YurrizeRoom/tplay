package com.yurrize.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yurrize.dto.WantPlayDto;
import com.yurrize.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Mapper接口
 * @author 缘昔
 * @since 2022.11.9
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    List<WantPlayDto> queryUserSimple(@Param("id") Long id);

}
