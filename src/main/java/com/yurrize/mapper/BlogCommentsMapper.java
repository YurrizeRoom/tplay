package com.yurrize.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yurrize.entity.BlogComments;
import org.springframework.stereotype.Repository;

/**
 * Mapper接口
 * @author 缘昔
 * @since 2022.11.9
 */
@Repository
public interface BlogCommentsMapper extends BaseMapper<BlogComments> {
}
