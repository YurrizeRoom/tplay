package com.yurrize.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yurrize.entity.Blog;
import org.springframework.stereotype.Repository;

/**
 * Mapper接口
 * @author 缘昔
 * @since 2022.11.9
 */
@Repository
public interface BlogMapper extends BaseMapper<Blog> {
}
