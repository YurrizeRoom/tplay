package com.yurrize.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yurrize.entity.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * Mapper接口
 * @author 缘昔
 * @since 2022.11.29
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
