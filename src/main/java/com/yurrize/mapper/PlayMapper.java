package com.yurrize.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yurrize.entity.Play;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Mapper接口
 * @author 缘昔
 * @since 2022.11.9
 */
@Repository
public interface PlayMapper extends BaseMapper<Play> {
    List<String> queryName();
}
