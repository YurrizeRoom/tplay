# 一起玩

# 数据库设计

DaseBase: tplay

表头：tb_

tb_user表（记录用户信息）

```sql
create table tplay.tb_user
(
    id          bigint unsigned auto_increment comment '主键'
        primary key,
    phone       varchar(11)                            not null comment '手机号码',
    nick_name   varchar(32)  default ''                null comment '昵称，默认是用户id',
    icon        varchar(255) default ''                null comment '人物头像',
    create_time timestamp    default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time timestamp    default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间',
    constraint uniqe_key_phone
        unique (phone)
)
    collate = utf8mb4_general_ci;

```

tb_play_type（游乐项目类别）

```sql
create table tplay.tb_play_type
(
    id          bigint unsigned auto_increment comment '主键'
        primary key,
    name        varchar(32)                         null comment '类型名称',
    icon        varchar(255)                        null comment '图标',
    sort        int unsigned                        null comment '顺序',
    create_time timestamp default CURRENT_TIMESTAMP null comment '创建时间',
    update_time timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '更新时间'
)
    collate = utf8mb4_general_ci;


```

tb_play（游乐项目）

```sql
create table tplay.tb_play
(
    id          bigint unsigned auto_increment comment '主键'
        primary key,
    name        varchar(128)                        not null comment '活动名称',
    user_id     bigint unsigned                     not null comment '活动创建的userId',
    type_id     bigint unsigned                     not null comment '活动类型的id',
    images      varchar(1024)                       not null comment '活动图片，多个图片以'',''隔开',
    address     varchar(255)                        not null comment '活动详细地址',
    x           double unsigned                     not null comment '经度',
    y           double unsigned                     not null comment '维度',
    avg_price   bigint unsigned                     null comment '活动均价，取整数',
    need_number        int unsigned zerofill               not null comment '活动需要的人',
    content     varchar(2048) collate utf8mb4_unicode_ci not null comment '活动的文字描述',
	begin_time  timestamp default CURRENT_TIMESTAMP not null comment '活动生效时间',
    end_time    timestamp default CURRENT_TIMESTAMP not null comment '活动失效时间',
    create_time timestamp default CURRENT_TIMESTAMP null comment '创建时间',
    update_time timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '更新时间'
)
    collate = utf8mb4_general_ci;
```

tb_want_player（活动报名人员表）

```sql
create table tplay.tb_want_player
(
    id          bigint unsigned auto_increment comment '主键'
        primary key,
    phone       varchar(11)                            not null comment '手机号码',
    city        varchar(64)      default ''                null comment '城市名称',
    nick_name   varchar(32)  default ''                null comment '姓名，默认是用户id',
    icon        varchar(255) default ''                null comment '个人照片',
    gender      tinyint unsigned default '0'               null comment '性别，0：男，1：女',
    birthday    date                                       null comment '生日',
    content     varchar(2048) collate utf8mb4_unicode_ci not null comment '个人介绍',
    status       tinyint unsigned default '1'               not null comment '1，申请中; 2,申请通过; 3,禁止参与'
)
    collate = utf8mb4_general_ci;
 

```

tb_blog

```sql
CREATE TABLE `tb_blog` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `play_id` bigint NOT NULL COMMENT '活动id',
  `user_id` bigint unsigned NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `images` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动的照片，最多9张，多张以","隔开',
  `content` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '活动心得',
  `liked` int unsigned DEFAULT '0' COMMENT '点赞数量',
  `comments` int unsigned DEFAULT NULL COMMENT '评论数量',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=COMPACT


```

tb_blog_comments

```sql
CREATE TABLE `tb_blog_comments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint unsigned NOT NULL COMMENT '用户id',
  `blog_id` bigint unsigned NOT NULL COMMENT '朋友圈id',
  `parent_id` bigint unsigned NOT NULL COMMENT '关联的1级评论id，如果是一级评论，则值为0',
  `answer_id` bigint unsigned NOT NULL COMMENT '回复的评论id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '回复的内容',
  `liked` int unsigned DEFAULT NULL COMMENT '点赞数',
  `status` tinyint unsigned DEFAULT NULL COMMENT '状态，0：正常，1：被举报，2：禁止查看',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=COMPACT


```



# 开发框架搭建

	运行sql文件，下载源码，修改本地数据库信息



# Play功能

用户登陆注册最后才写，直接往数据库注入自己的信息，用作登录
实现功能：

发布paly功能的实现：

涉及表格：tb_Play

实现功能，能使用postman等工具，对play进行发布

## POST 创建活动

### 逻辑

核心功能，上传发布活动，前端提交Post请求，请求路径为**/play/save**，传递指定参数，图片需要进行字符串的切割，将文件保存路径转换成String传上来

### 路径

POST /:8081/play/save

### 请求参数

| 名称         | 位置 | 类型    | 必选 | 说明 |
| ------------ | ---- | ------- | ---- | ---- |
| body         | body | object  | 否   | none |
| » name       | body | string  | 是   | none |
| » userId     | body | integer | 是   | none |
| » typeId     | body | integer | 是   | none |
| » images     | body | string  | 是   | none |
| » address    | body | string  | 是   | none |
| » x          | body | integer | 是   | none |
| » y          | body | integer | 是   | none |
| » avgPrice   | body | integer | 是   | none |
| » needNumber | body | integer | 是   | none |
| » content    | body | string  | 是   | none |
| » beginTime  | body | string  | 是   | none |
| » endTime    | body | string  | 是   | none |

> 返回示例

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |



## GET 分页根据userId查询活动

### 逻辑

点击个人页面，显示该登录用户名下所有的自己创建的活动，current为分页查询的参数，默认是1，一页十条数据

### 路径

GET /:8081/play/of/user

分页查询，根据userId查询到用户名下所创建的活动

### 请求参数

| 名称    | 位置  | 类型   | 必选 | 说明 |
| ------- | ----- | ------ | ---- | ---- |
| userId  | query | string | 是   | none |
| current | query | string | 是   | none |

> 返回示例

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |



## GET 分页根据typeId查询活动

GET /:8081/play/of/type

### 请求参数

| 名称    | 位置  | 类型   | 必选 | 说明 |
| ------- | ----- | ------ | ---- | ---- |
| typeId  | query | string | 是   | none |
| current | query | string | 是   | none |

> 返回示例

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

## GET 首页查询活动

GET /:8081/play/hot

### 请求参数

| 名称    | 位置  | 类型   | 必选 | 说明 |
| ------- | ----- | ------ | ---- | ---- |
| current | query | string | 是   | none |

> 返回示例

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |



# User功能

## GET 登录功能



### 逻辑

​	微信小程序调用API wx.login发起GET请求，路径为/user/login，请求会得到一串code，使用回调传回服务器，服务器返回唯一登录凭证，用作后续登录认证

### 路径

/user/login

### 请求参数

### 请求参数

| 名称 | 位置  | 类型   | 必选 | 说明 |
| ---- | ----- | ------ | ---- | ---- |
| code | query | string | 否   | none |

### 返回参数

唯一登录凭证id OpenId

## 登录验证功能（后台实现）

### 逻辑

获取后台提供的oppenId后，在每一次请求头中加入，Authorization字段，String类型，存放oppenId，后端每次通过此OppenId进行是否登录验证，若为null则拦截请求，返回请求登录信息，前端须根据返回的信息进行重新调用登录函数再次登录

### 返回参数

当收到**false，请先登录**

让用户跳转到登录页面，调用wx.login



## POST 发送验证码功能

### 逻辑

用户首先要通过登录，请求能正常使用的前提请求头中含有Authorization字段，String类型，存在oppenId，登录验证通过后，在个人主页绑定用户的手机号码

### 路径

点击发送验证码：/user/code

填写完手机号以及验证码后提交表单：/user/bind

### 请求参数

| 名称          | 位置   | 类型   | 必选 | 说明 |
| ------------- | ------ | ------ | ---- | ---- |
| phone         | query  | string | 是   | none |
| Authorization | header | string | 否   | none |



### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |



## POST 绑定手机号

### 逻辑

用户登录后，绑定手机号，请求能正常使用的前提请求头中含有Authorization字段，String类型，存在oppenId，当用户获取完登陆验证码后，填写验证码，提交表单信息，表单信息含手机号和表单验证码进行校验

### 路径

/user/bind

> Body 请求参数

```json
{
  "phone": "string",
  "code": "string"
}
```

### 请求参数

| 名称          | 位置   | 类型   | 必选 | 说明 |
| ------------- | ------ | ------ | ---- | ---- |
| Authorization | header | string | 否   | none |
| body          | body   | object | 否   | none |
| » phone       | body   | string | 是   | none |
| » code        | body   | string | 是   | none |

> 返回示例

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |
